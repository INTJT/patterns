import {SocketHelper} from "../helpers/socketHelper.js";
import {UserService} from "./userService.js";
import {RoomsService} from "./roomsService.js";
import {gameEvents, roomStatutes, url} from "../constants.js";
import {ObservableMap, ObservableValue} from "../observable.js";
import {CommentatorService} from "./commentatorService.js";

export class GameService {

    static #socket = new ObservableValue();
    static #name = new ObservableValue();
    static #status = new ObservableValue();
    static #text = new ObservableValue();
    static #timer = new ObservableValue();
    static #users = new ObservableMap();

    static #timerId;

    static get socket() { return GameService.#socket; }
    static get name() { return GameService.#name; }
    static get users() { return GameService.#users; }
    static get status() { return GameService.#status; }
    static get timer() { return GameService.#timer; }
    static get text() { return GameService.#text; }

    static setUp(room) {
        GameService.name.value = room.name;
        GameService.status.value = roomStatutes.WAITING;
        for (const user of room.users) GameService.#users.set(user.name, user);
    }

    static connect(query) {
        if(GameService.socket.value) GameService.disconnect();
        return SocketHelper.connect("/race", query, socket => {
            socket.on(gameEvents.SET_UP, game => GameService.setUp(game));
            socket.on(gameEvents.JOIN, user => GameService.users.set(user.name, user));
            socket.on(gameEvents.UPDATE_USER, user => GameService.users.set(user.name, user));
            socket.on(gameEvents.LEAVE, user => GameService.users.delete(user.name));
            socket.on(gameEvents.START_TIMER, ({timer, index, commentator}) => {
                GameService.status.value = roomStatutes.TIMER;
                GameService.countDown(new Date(timer));
                fetch(`${url}/game/texts/${index}`)
                    .then(response => response.text().then(text => {
                        GameService.text.value = text;
                    }));
                CommentatorService.commentator = commentator;
            });
            socket.on(gameEvents.START_GAME, timer => {
                GameService.status.value = roomStatutes.GAME;
                GameService.countDown(new Date(timer));
            });
            socket.on(gameEvents.COMMENT, comment => {
                CommentatorService.comment = comment;
            });
        })
            .then(socket => {
                GameService.socket.value = socket;
                RoomsService.disconnect();
                return socket;
            });
    }

    static join(name) {
        return GameService.connect({
            username: UserService.username,
            roomName: name
        });
    }

    static create(name) {
        return GameService.connect({
           username: UserService.username,
           roomName: name,
           create: true
        });
    }

    static disconnect() {
        if(GameService.socket.value) {
            GameService.socket.value.disconnect();
            GameService.socket.value = undefined;
            GameService.name.value = undefined;
            GameService.text.value = undefined;
            GameService.status.value = undefined;
            GameService.timer.value = undefined;
            GameService.clearCountDown();
            GameService.users.clear();
            CommentatorService.commentator.value = undefined;
        }
    }

    static clearCountDown() {
        if(GameService.#timerId) {
            clearInterval(GameService.#timerId);
            GameService.#timerId = undefined;
        }
    }

    static countDown(date) {
        GameService.clearCountDown();
        function tick() {
            const now = new Date();
            if (now >= date && id === GameService.#timerId) GameService.clearCountDown();
            else GameService.timer.value = Math.ceil((date - now) / 1000);
        }
        const id = GameService.#timerId = setInterval(tick, 1000);
        tick();
    }

}
