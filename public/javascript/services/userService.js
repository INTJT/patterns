import {SocketHelper} from "../helpers/socketHelper.js";
import {errors} from "../constants.js";

export class UserService extends SocketHelper {

    static #socket;
    static #username = sessionStorage.getItem("username");

    static get username() { return UserService.#username; }

    static connect() {
        if(UserService.username) {
            return SocketHelper.connect("/users", { username: UserService.username })
                .then(socket => {
                    UserService.#socket = socket;
                    return socket;
                });
        }
        else return Promise.reject(new Error(errors.NO_USERNAME));
    }

    static disconnect() {
        if(UserService.#socket) {
            UserService.#socket.disconnect();
            UserService.#socket = undefined;
        }
    }

    static toLogin() {
        UserService.disconnect();
        sessionStorage.clear();
        window.location.replace("/login");
    }

}
