import {SocketHelper} from "../helpers/socketHelper.js";
import {roomsEvents} from "../constants.js";
import {GameService} from "./gameService.js";
import {ObservableMap, ObservableValue} from "../observable.js";

export class RoomsService {

    static #socket = new ObservableValue();
    static #rooms = new ObservableMap();

    static get socket() { return RoomsService.#socket; }
    static get rooms() { return RoomsService.#rooms; }

    static connect() {
        if(RoomsService.socket.value) RoomsService.disconnect();
        return SocketHelper.connect("/rooms", {}, socket => {
            socket.on(roomsEvents.UPDATE_ROOMS, rooms => {
                RoomsService.rooms.clear();
                rooms.forEach(room => RoomsService.rooms.set(room.name, room));
            });
            socket.on(roomsEvents.ADD_ROOM, room => RoomsService.rooms.set(room.name, room));
            socket.on(roomsEvents.UPDATE_ROOM, room => RoomsService.rooms.set(room.name, room));
            socket.on(roomsEvents.REMOVE_ROOM, room => RoomsService.rooms.delete(room.name));
        })
            .then(socket => {
                RoomsService.socket.value = socket;
                GameService.disconnect();
                return socket;
            });
    }

    static disconnect() {
        if(RoomsService.socket.value) {
            RoomsService.socket.value.disconnect();
            RoomsService.socket.value = undefined;
            RoomsService.rooms.clear();
        }
    }

}
