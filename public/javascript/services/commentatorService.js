import {ObservableValue} from "../observable.js";

export class CommentatorService {

    static #commentator = new ObservableValue();
    static #comment = new ObservableValue();

    static get commentator() { return CommentatorService.#commentator; }
    static get comment() { return CommentatorService.#comment; }

    static set commentator(commentator) {
        CommentatorService.#commentator.value = commentator;
    }
    static set comment(comment) {
        CommentatorService.#comment.value = comment;
    }

}
