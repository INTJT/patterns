import {errorsDescriptions, gameEvents, roomStatutes, url} from "./constants.js";
import {UserService} from "./services/userService.js";
import {RoomsService} from "./services/roomsService.js";
import {GameService} from "./services/gameService.js";
import {div, span, button, h1, h3, small} from "./helpers/componentHelper.js";
import {mapEvents, valueEvents} from "./observable.js";
import {CommentatorService} from "./services/commentatorService.js";

if(!UserService.username) UserService.toLogin();
UserService.connect().catch(error => {
    alert(errorsDescriptions[error.message] || error.message);
    UserService.toLogin();
});

RoomsService.connect().then();

function findByName(container, name) {
    return container.querySelector(`[data-name="${name}"]`);
}

function addElement(container, element) { container.appendChild(element); }

function updateElement(container, element) {
    const previousElement = findByName(container, element.dataset.name);
    if(previousElement) container.replaceChild(element, previousElement);
    else container.appendChild(element);
}

function deleteElement(container, name) {
    const element = findByName(container, name);
    if(element) container.removeChild(element);
}

function clearContainer(container) {
    container.innerHTML = "";
}

function setHidden(element, value) {
    if (value) element.classList.add("display-none");
    else element.classList.remove("display-none");
}

//setup rooms page
{

    const roomsPage = document.getElementById("rooms-page");
    const roomsList = document.getElementById("rooms-list");

    const RoomCard = room => div({
        className: ["room", "card"],
        attributes: { ["data-name"]: room.name }
    });
    const CardTitle = h1({ className: ["card-title", "one-line"] });
    const JoinButton = (room) => {
        const element = button({ className: ["join", "no-select"] })("Join");
        element.onclick = () => GameService.join(room.name);
        return element;
    }

    function RoomElement(room) {
        return RoomCard(room)(
            small()(`${room.users.length} users`),
            CardTitle(room.name),
            JoinButton(room)
        );
    }

    RoomsService.socket.on(valueEvents.set, event => setHidden(roomsPage, !Boolean(event.newValue)));
    RoomsService.rooms.on(mapEvents.add, event => addElement(roomsList, RoomElement(event.value)));
    RoomsService.rooms.on(mapEvents.update, event => updateElement(roomsList, RoomElement(event.value)));
    RoomsService.rooms.on(mapEvents.delete, event => deleteElement(roomsList, event.key));
    RoomsService.rooms.on(mapEvents.clear, () => clearContainer(roomsList));

}

//setup game page
{

    const readyIcon = user => span({ className: ["icon", user.ready ? "ready-status-green" : "ready-status-red"] })();

    function UserElement(user) {

        const classes = ["user"];
        const percent = (user.typing || 0) / (GameService.text.value?.length || 1);
        if(percent === 1) classes.push("complete");

        return div({ className: classes, attributes: { ["data-name"]: user.name } })(
            h3({ className: "one-line" })(
                readyIcon(user),
                user.name,
                user.name === UserService.username ? small()("(you)") : null
            ),
            div({ className: ["user-progress", user.name] })(
                span({ attributes: { style: `width: ${percent * 100}%` } })()
            )
        );

    }

    const gamePage = document.getElementById("game-page");
    const roomName = document.getElementById("room-name");
    const usersList = document.getElementById("users-list");
    const textContainer = document.getElementById("text-container");
    const timer = document.getElementById("timer");
    const quitButton = document.getElementById("quit-room-btn");
    const readyButton = document.getElementById("ready-btn");

    function type(event) {
        const typing = GameService.users.get(UserService.username).typing;
        if(GameService.text.value[typing] === event.key) {
            GameService.socket.value.emit(gameEvents.TYPE_CHAR, event.key);
        }
    }

    function updateText() {
        const text = GameService.text.value;
        if(text) {
            const typing = GameService.users.get(UserService.username).typing || 0;
            textContainer.innerHTML = `<span class="text-written">${text.substring(0, typing)}</span>${
                typing < text.length ? `<span class="text-current">${text[typing]}</span>${text.substring(typing + 1)}` : ""
            }`;
        }
    }

    GameService.socket.on(valueEvents.set, event => setHidden(gamePage, !Boolean(event.newValue)));
    GameService.name.on(valueEvents.set, event => {
        if(event.newValue) roomName.innerText = event.newValue;
        else roomName.innerText = "Game";
    });
    GameService.status.on(valueEvents.set, event => {
        switch(event.newValue) {
            case roomStatutes.WAITING:
                setHidden(timer, true);
                setHidden(textContainer, true);
                setHidden(readyButton, false);
                setHidden(quitButton, false);
                window.onkeyup = null;
                break;
            case roomStatutes.TIMER:
                setHidden(quitButton, true);
                setHidden(readyButton, true);
                setHidden(timer, false);
                timer.classList.remove("game-timer");
                timer.innerText = "";
                break;
            case roomStatutes.GAME:
                setHidden(textContainer, false);
                timer.classList.add("game-timer");
                timer.innerText = "";
                window.onkeyup = type;
                break;
        }
    });
    GameService.timer.on(valueEvents.set, event => {
        timer.innerText = GameService.status.value === roomStatutes.TIMER ? event.newValue : `${event.newValue} seconds left`;
    });
    GameService.text.on(valueEvents.set, () => updateText());

    GameService.users.on(mapEvents.add, event => addElement(usersList, UserElement(event.value)));
    GameService.users.on(mapEvents.update, event => {
        if(event.value.name === UserService.username) {
            if(GameService.status.value === roomStatutes.WAITING) {
                readyButton.innerText = event.value.ready ? "NOT READY" : "READY";
            }
            updateText();
        }
        updateElement(usersList, UserElement(event.value));
    });
    GameService.users.on(mapEvents.delete, event => deleteElement(usersList, event.key));
    GameService.users.on(mapEvents.clear, () => clearContainer(usersList));

    readyButton.onclick = () => GameService.socket.value.emit(gameEvents.TOGGLE_READY);

}

//setup commentator
{

    const commentatorBlock = document.getElementById("commentator");
    const name = document.getElementById("commentator-name");
    const profile = document.getElementById("commentator-profile");
    const comment = document.getElementById("commentator-comment");

    CommentatorService.commentator.on(valueEvents.set, event => {
        if(event.newValue) {
            setHidden(commentatorBlock, false);
            name.innerText = event.newValue.name;
            profile.src = `${url}/${event.newValue.profile}`;
        }
        else setHidden(commentatorBlock, true);
    });

    CommentatorService.comment.on(valueEvents.set, event => {
       comment.innerHTML = event.newValue;
    });

}

document.getElementById("add-room-btn").onclick = () => {
    const roomName = prompt("Room name: ");
    if(roomName) {
        GameService.create(roomName)
            .catch(error => alert(errorsDescriptions[error.message] || error.message));
    }
}

document.getElementById("quit-room-btn").onclick = () => {
    GameService.disconnect();
    RoomsService.connect().then();
}
