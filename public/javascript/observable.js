export class Observable {

    #groups = new Map();

    constructor(groups) {
        for(const group of groups) this.#groups.set(group, new Set());
    }

    on(group, callback) {
        if(!this.#groups.has(group)) {
            throw new Error(`Group "${group}" doesn't exist`);
        }
        (this.#groups.get(group)).add(callback);
    }

    notify(event) {
        if(!this.#groups.has(event.type)) {
            throw new Error(`Group "${event.type}" doesn't exist`);
        }
        this.#groups.get(event.type).forEach(callback => callback(event));
    }

}

//Observable value

export const valueEvents = {
    set: Symbol("SET")
}

export class ObservableValue extends Observable {

    #value;

    constructor(value) {
        super([valueEvents.set]);
        this.#value = value;
    }

    get value() { return this.#value; }

    set value(value) {
        if(this.#value !== value) {
            const oldValue = this.#value;
            this.#value = value;
            this.notify({
                type: valueEvents.set,
                oldValue,
                newValue: this.#value
            });
        }
    }

}

//Observable map

export const mapEvents = {
    add: Symbol("ADD"),
    update: Symbol("UPDATE"),
    delete: Symbol("DELETE"),
    clear: Symbol("CLEAR")
}

export class ObservableMap extends Observable {

    #map;

    constructor(map) {
        super([
            mapEvents.add,
            mapEvents.update,
            mapEvents.delete,
            mapEvents.clear
        ]);
        this.#map = new Map(map || []);
    }

    get(key) {
        return this.#map.get(key);
    }

    set(key, value) {
        const exists = this.has(key);
        this.#map.set(key, value);
        this.notify({
            type: exists ? mapEvents.update : mapEvents.add,
            key, value
        });
    }

    has(key) {
        return this.#map.has(key);
    }

    delete(key) {
        if (this.has(key)) {
            const value = this.get(key);
            this.#map.delete(key);
            this.notify({
                type: mapEvents.delete,
                key, value
            });
            return value;
        }
    }

    clear() {
        if(this.#map.size !== 0) {
            this.#map.clear();
            this.notify({
                type: mapEvents.clear
            });
        }
    }

    get size() {
        return this.#map.size;
    }

    array() { return Array.from(this.#map); }

}
