import {url} from "../constants.js";

export class SocketHelper {

    static connect(path, query = {}, init = () => {}) {
        return new Promise((resolve, reject) => {

            const socket = io(`${url}${path}`, {
                forceNew: true,
                query
            });

            init(socket);

            socket.on("connect", () => resolve(socket));
            socket.on("connect_error", error => reject(error));

        });
    }

}
