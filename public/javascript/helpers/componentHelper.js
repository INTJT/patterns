export class DomHelper {

    static #domElementToNode(domElement) {
        if(typeof domElement === "string") return document.createTextNode(domElement);
        else return domElement;
    }

    static createTag(tag) {
        return function(attributes) {
            return function(...inner) {

                const element = document.createElement(tag);

                if(attributes) {

                    if (attributes.className !== undefined) {
                        if (Array.isArray(attributes.className)) element.classList.add(...attributes.className);
                        else element.className = attributes.className;
                    }

                    if(attributes.id !== undefined) element.id = attributes.id;

                    if(attributes.attributes) {
                        for(const [attribute, value] of Object.entries(attributes.attributes)) {
                            element.setAttribute(attribute, value);
                        }
                    }

                }

                inner
                    .forEach(node => {
                        if(node !== null && node !== undefined) {
                            element.appendChild(DomHelper.#domElementToNode(node))
                        }
                    });

                return element;

            }
        }
    }

}

export const div = DomHelper.createTag("div");
export const span = DomHelper.createTag("span");
export const h1 = DomHelper.createTag("h1");
export const h3 = DomHelper.createTag("h3");
export const button = DomHelper.createTag("button");
export const small = DomHelper.createTag("small");
