export const url = "http://localhost:3002";

export const roomsEvents = {
    UPDATE_ROOMS: "UPDATE_ROOMS",
    ADD_ROOM: "ADD_ROOM",
    UPDATE_ROOM: "UPDATE_ROOM",
    REMOVE_ROOM: "REMOVE_ROOM"
}

export const gameEvents = {
    SET_UP: "SET_UP",
    JOIN: "JOIN",
    LEAVE: "LEAVE",
    UPDATE_USER: "UPDATE_USER",
    TOGGLE_READY: "TOGGLE_READY",
    START_TIMER: "START_TIMER",
    START_GAME: "START_GAME",
    TYPE_CHAR: "TYPE_CHAR",
    COMMENT: "COMMENT"
}

export const errors = {
    NO_USERNAME: "NO_USERNAME",
    NO_ROOM: "NO_ROOM",
    USER_ALREADY_EXISTS: "USER_ALREADY_EXISTS",
    ROOM_ALREADY_EXISTS: "ROOM_ALREADY_EXISTS",
    USER_NOT_FOUND: "USER_NOT_FOUND",
    ROOM_NOT_FOUND: "ROOM_NOT_FOUND",
    ALREADY_IN_ROOM: "ALREADY_IN_ROOM",
    ALREADY_STARTED: "ALREADY_STARTED",
    TOO_MANY_USERS: "TOO_MANY_USERS"
}

export const errorsDescriptions = {
    NO_USERNAME: "Please choose username",
    NO_ROOM: "Please choose the room",
    USER_ALREADY_EXISTS: "Username with that username already exists",
    ROOM_ALREADY_EXISTS: "The room with that name already exists",
    USER_NOT_FOUND: "User not found",
    ROOM_NOT_FOUND: "Room not found",
    ALREADY_IN_ROOM: "You already in the room",
    ALREADY_STARTED: "Game in the room already started",
    TOO_MANY_USERS: "The room is full"
}

export const roomStatutes = {
    WAITING: "WAITING",
    TIMER: "TIMER",
    GAME: "GAME"
}
