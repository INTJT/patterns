import path from "path";

export const PORT: number = 3002;

export const DATA_PATH: string = path.join(__dirname, "../data");
export const PUBLIC_PATH: string = path.join(__dirname, "../public");
export const HTML_PATH: string = path.join(PUBLIC_PATH, "html");
