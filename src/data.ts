import {Repository, CachedRepository, JsonRepository} from "./core/repository";
import path from "path";
import {DATA_PATH} from "./config";

import {CommentatorInfo} from "./core/info";

export const comments = new CachedRepository(new JsonRepository<string>(path.join(DATA_PATH, "comments.json")));
export const commentators = new CachedRepository(new JsonRepository<CommentatorInfo>(path.join(DATA_PATH, "commentators.json")));
export const racers = new CachedRepository(new JsonRepository<string>(path.join(DATA_PATH, "racers.json")));
export const texts = new CachedRepository(new JsonRepository<string>(path.join(DATA_PATH, "texts.json")));

import _ from "lodash";

export function randomFromRepository<I>(repository: Repository<I>): (category: string) => I {
    return function (category: string): I {
        const content = repository.getCategory(category);
        if(content !== undefined && content.length !== 0) return _.sample(content) as I;
        else throw new Error(`${category} doesn't exists`);
    }
}
