import socketIO from "socket.io";
import {namespaces} from "./namespaces";
import {usersRoute} from "./socketRoutes/usersRoute";
import {roomsRoute} from "./socketRoutes/roomsRoute";
import {raceRoute} from "./socketRoutes/raceRoute";

export default (io: socketIO.Server) => {
    usersRoute(io.of(namespaces.users));
    const createRoom = roomsRoute(io.of(namespaces.rooms));
    raceRoute(io.of(namespaces.race), createRoom);
};
