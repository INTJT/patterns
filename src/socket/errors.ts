export enum errors {
    NoUsername = "NO_USERNAME",
    NoRoom = "NO_ROOM",
    UserAlreadyExists = "USER_ALREADY_EXISTS",
    RoomAlreadyExists = "ROOM_ALREADY_EXISTS",
    UserNotFound = "USER_NOT_FOUND",
    RoomNotFound = "ROOM_NOT_FOUND",
    AlreadyInRoom = "ALREADY_IN_ROOM",
    AlreadyStarted = "ALREADY_STARTED",
    TooManyUsers = "TOO_MANY_USERS"
}
