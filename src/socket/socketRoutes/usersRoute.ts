import {KeyboardRaces, User} from "../../core";
import {errors} from "../errors";
import socketIO from "socket.io";

export const users: User[] = [];

export interface PublicUser {
    name: string,
    ready: boolean,
    typing: number
}

export function usersRoute(usersNamespace: socketIO.Namespace) {

    usersNamespace.use((socket, next) => {
        const username = socket.handshake.query.username;
        if(username === undefined) next(new Error(errors.NoUsername));
        if (users.find(user => user.name === username)) next(new Error(errors.UserAlreadyExists));
        else next();
    });

    usersNamespace.on("connection", (socket: socketIO.Socket) => {

        const user = KeyboardRaces.createUser(socket.handshake.query.username as string);
        users.push(user);

        socket.on("disconnect", () => {
            users.splice(users.indexOf(user), 1);
        });

    });

}
