import {KeyboardRaces, Observer, Room, RoomEvent, User} from "../../core";
import socketIO from "socket.io";
import {PublicUser} from "./usersRoute";

export const rooms: Room[] = [];

export enum RoomsSocketEvent {
    UpdateRooms = "UPDATE_ROOMS",
    AddRoom = "ADD_ROOM",
    UpdateRoom = "UPDATE_ROOM",
    RemoveRoom = "REMOVE_ROOM"
}

export interface PublicRoom {
    name: string,
    users: PublicUser[]
}

export function roomToPublic(room: Room): PublicRoom {
    return {
        name: room.name,
        users: room.users.map(user => userToPublic(user, room))
    };
}

export function userToPublic(user: User, room: Room): PublicUser {
    return {
        name: user.name,
        ready: room.isReady(user),
        typing: room.race && room.users.includes(user) ?
            (room.race.typing.has(user) ? room.race.typing.get(user) as number : room.race.text.length) : 0
    }
}

export function roomsRoute(roomsNamespace: socketIO.Namespace) {

    roomsNamespace.on("connection", socket => {
        socket.emit(RoomsSocketEvent.UpdateRooms, rooms.filter(room => room.visible).map(room => roomToPublic(room)));
    });

    return function createRoom(name: string): Room {
        const roomsIndex = rooms.findIndex(r => r.name === name);
        if(roomsIndex === -1) {

            const room = KeyboardRaces.createRoom(name);
            rooms.push(room);

            function update() {
                roomsNamespace.emit(RoomsSocketEvent.UpdateRoom, roomToPublic(room));
            }

            const observer = new Observer();

            observer.subscribe(room, RoomEvent.Join, update);
            observer.subscribe(room, RoomEvent.Leave, update);
            observer.subscribe(room, RoomEvent.ChangeVisible, (visible: boolean) => {
                roomsNamespace.emit(visible ? RoomsSocketEvent.AddRoom : RoomsSocketEvent.RemoveRoom, roomToPublic(room));
            });
            observer.subscribe(room, RoomEvent.Empty, () => {
                const index = rooms.indexOf(room);
                if(index !== -1) rooms.splice(index, 1);
                roomsNamespace.emit(RoomsSocketEvent.RemoveRoom, roomToPublic(room));
                observer.unsubscribe();
            });

            return room;

        }
        else throw new Error(`Trying add room "${name}" that already exists`);
    }

}
