import socketIO from "socket.io";
import {errors} from "../errors";
import {users} from "./usersRoute";
import {rooms, roomToPublic, userToPublic} from "./roomsRoute";
import {Commentator, KeyboardRaces, Observer, Race, Room, RoomEvent, RoomState, User} from "../../core";

export enum RaceSocketEvents {
    SetUp = "SET_UP",
    Join = "JOIN",
    Leave = "LEAVE",
    UpdateUser = "UPDATE_USER",
    ToggleReady = "TOGGLE_READY",
    StartTimer = "START_TIMER",
    StartRace = "START_GAME",
    TypeChar = "TYPE_CHAR",
    Comment = "COMMENT"
}

interface PublicCommentator {
    name: string;
    profile: string;
}

function commentatorToPublic(commentator: Commentator): PublicCommentator {
    return {
        name: commentator.name,
        profile: commentator.profile
    }
}

export function raceRoute(raceNamespace: socketIO.Namespace, createRoom: (name: string) => Room) {

    raceNamespace.use((socket, next) => {

        const {username, roomName, create} = socket.handshake.query;

        //must have username param
        if(!username) {
            next(new Error(errors.NoUsername));
            return;
        }

        const user = users.find(user => user.name === username);

        //user must be in users
        if(!user) {
            next(new Error(errors.UserNotFound));
            return;
        }

        //must have roomName param
        if(typeof roomName !== "string" || roomName === "") {
            next(new Error(errors.NoRoom));
            return;
        }

        const room = rooms.find(room => room.name === roomName);

        //room shouldn't exists if create is true and vise versa
        if(Boolean(room) === Boolean(create)) {
            next(new Error(create ? errors.RoomAlreadyExists : errors.NoRoom));
            return;
        }

        if(!create) {

            //user can't join the room if game is started
            if(room?.state !== RoomState.Waiting) {
                next(new Error(errors.AlreadyStarted));
                return;
            }

            //user can't join the room if room is full
            if(room.isFull) {
                next(new Error(errors.TooManyUsers));
                return;
            }

        }

        next();

    });

    function observeRace(room: Room): void {

        const roomObserver = new Observer();

        function startIfReady() {
            if(room.allReady && room.state === RoomState.Waiting) {

                const [race, index] = KeyboardRaces.createRace(room);
                const raceObserver = new Observer();

                raceObserver.subscribe(room, RoomEvent.StartTimer, (race: Race, timer: Date) => {
                    raceNamespace.to(room.name).emit(RaceSocketEvents.StartTimer, {timer, index, commentator: commentatorToPublic(commentator)});
                });

                raceObserver.subscribe(room, RoomEvent.StartRace, (race: Race, timer: Date) => {
                    raceNamespace.to(room.name).emit(RaceSocketEvents.StartRace, timer);
                });

                raceObserver.subscribe(room, RoomEvent.EndRace, () => {
                    raceNamespace.to(room.name).emit(RaceSocketEvents.SetUp, roomToPublic(room));
                    raceObserver.unsubscribe();
                });

                const commentator = KeyboardRaces.createCommentator(race, (comment: string) => {
                    raceNamespace.to(room.name).emit(RaceSocketEvents.Comment, comment);
                });

                room.startRace(race);

            }
        }

        roomObserver.subscribe(room, RoomEvent.ChangeReady, startIfReady);
        roomObserver.subscribe(room, RoomEvent.Leave, startIfReady);
        roomObserver.subscribe(room, RoomEvent.Empty, () => roomObserver.unsubscribe());

    }

    raceNamespace.on("connection", socket => {

        const { username, create } = socket.handshake.query;
        const roomName = socket.handshake.query.roomName as string;

        const user = users.find(user => user.name === username) as User;
        const room = create ? createRoom(roomName) : rooms.find(room => room.name === roomName) as Room;

        if(create) observeRace(room);

        room.join(user);

        socket.join(roomName);
        socket.emit(RaceSocketEvents.SetUp, roomToPublic(room));
        raceNamespace.to(roomName).emit(RaceSocketEvents.Join, user);

        socket.on(RaceSocketEvents.ToggleReady, () => {
            if(room.state === RoomState.Waiting) {
                room.setReady(user, !room.isReady(user));
                raceNamespace.to(roomName).emit(RaceSocketEvents.UpdateUser, userToPublic(user, room));
            }
        });

        socket.on(RaceSocketEvents.TypeChar, char => {
            if(room.state === RoomState.Race) {
                const typing = (room.race as Race).type(user, char);
                if(typeof typing === "number") raceNamespace.to(room.name).emit(RaceSocketEvents.UpdateUser, userToPublic(user, room));
            }
        });

        socket.on("disconnect", () => {
            room.leave(user);
            raceNamespace.to(roomName).emit(RaceSocketEvents.Leave, user);
        });

    });

}
