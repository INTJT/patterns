export const MAXIMUM_USERS_FOR_ONE_ROOM = 5;
export const SECONDS_TIMER_BEFORE_START_GAME = 15;
export const SECONDS_FOR_GAME = 120;
export const SECONDS_TIMER_FOR_NOW_COMMENTS = 30;
export const CLOSE_TYPING = 30;
