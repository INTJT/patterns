import {Commentator, CommentatorEvent, Message} from "./commentator";
import {User} from "./user";
import {Race} from "./race";
import {Observable} from "./observable";
import {CommentatorFactory} from "./factory";

import {commentators, comments, randomFromRepository} from "../data";
import {CommentatorInfo, fullRacerInfo, improviseRacerInfo, RacerInfo, randomRacerInfo} from "./info";
import _ from "lodash";
import {Room} from "./room";
import {SECONDS_FOR_GAME} from "../constants";

const randomCommentator = randomFromRepository(commentators);
const randomComment = randomFromRepository(comments);

function render(template: string, obj: object) {
    const executor = _.template(template);
    return executor(obj);
}

class HtmlCommentator extends Observable implements Commentator {

    readonly name: string;
    readonly profile: string;

    readonly race: Race;
    readonly room: Room;

    readonly racersInfo: Map<User, RacerInfo>;

    constructor(race: Race) {

        super(...Object.values(CommentatorEvent));

        const commentatorInfo: CommentatorInfo = randomCommentator("commentators");
        this.name = commentatorInfo.name;
        this.profile = commentatorInfo.profile;

        this.racersInfo = new Map<User, RacerInfo>(race.room.users.map(user => [user, improviseRacerInfo(user)]));

        this.race = race;
        this.room = race.room;

    }

    comment(message: Message): void {
        this.emit(CommentatorEvent.Comment, message);
    }

    greetings(): Message {
        return render(randomComment("greetings"), { name: this.name })
    }

    info(): Message {
        const racers = this.room.users.map(user => fullRacerInfo(this.racersInfo.get(user) as RacerInfo)).join(". ");
        return render(randomComment("info"), { racers });
    }

    start(): Message {
        return randomComment("start");
    }

    now(): Message {
        const racers = this.placesToString(this.race.places, "now");
        const remaining = this.race.starts ?
            Math.round((+this.race.starts
                + SECONDS_FOR_GAME * 1000
                -(+new Date())) / 1000) : SECONDS_FOR_GAME;
        return render(randomComment("now"), { racers, remaining });
    }

    close(): Message {
        const racers = this.placesToString(this.race.places.slice(0, 3), "close");
        return render(randomComment("close"), { racers });
    }

    leave(user: User): Message {
        return render(randomComment("leave"), { racer: randomRacerInfo(this.racersInfo.get(user) as RacerInfo) });
    }

    finish(user: User, place: number): Message {
        const finishCategory = `${this.placeToCategory(place)}-finish`;
        return render(randomComment(finishCategory), { racer: randomRacerInfo(this.racersInfo.get(user) as RacerInfo) })
    }

    results(): Message {
        const results = this.race.places.map(user => {
            const now = new Date();
            const time = new Date(
                +(this.race.finished.get(user) || now) -
                +(this.race.starts || now)
            ).toISOString().substr(11, 8);
            return `<li>${user.name} - ${time}</li>`;
        }).join("");
        return render(randomComment("results"), { results });
    }

    joke(): Message {
        const category = _.sample(["jokes", "stories", "facts"]) as string;
        return randomComment(category);
    }

    private placesToString(places: User[], state: string): string {

        return places.map((user, index) => {

            const category = `${this.placeToCategory(index + 1)}-${state}`;
            const front = places[index - 1];
            const distance = (front ? (this.race.typing.get(front) as number) : this.race.text.length) - (this.race.typing.get(user) as number);

            return render(randomComment(category), {
                racer: randomRacerInfo(this.racersInfo.get(user) as RacerInfo),
                place: index + 1,
                front: front?.name,
                distance
            });

        }).join(". ");

    }

    private placeToCategory(place: number): string {
        switch (place) {
            case 1:
                return "first";
            case 2:
                return "second";
            case 3:
                return "third"
            case (this.room.users.length):
                return "last";
            default:
                return "other"
        }
    }

}

export class HtmlCommentatorFactory implements CommentatorFactory {

    createCommentator(race: Race): Commentator {
        return new HtmlCommentator(race);
    }

}
