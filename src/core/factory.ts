import {Race} from "./race";
import {Commentator} from "./commentator";

export interface CommentatorFactory {
    createCommentator(race: Race): Commentator
}
