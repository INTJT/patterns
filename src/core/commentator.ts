import {User} from "./user";
import {Observable} from "./observable";

export type Message = string;

export enum CommentatorEvent {
    Comment = "COMMENT"
}

export interface Commentator extends Observable {

    name: string;
    profile: string;

    // According to the Ukrainian Keyboard Racing Federation rules,
    // Commentator must comment on all stages of the race from startRace to finish:
    comment(message: Message): void;

    greetings(): Message;                         // 1. greet and introduce yourself
    info(): Message;                              // 2. announce racer participants
    start(): Message;                             // 3. report that game starts
    now(): Message;                               // 4. report the current state every 30 seconds
    close(): Message;                             // 5. report when racers are close to finish race
    finish(user: User, place: number): Message;   // 6. report that user finished the race
    leave(user: User): Message;                   // 7. report that user have left the race
    results(): Message;                           // 8. announce results
    joke(): Message;                              // 9. arbitrarily insert jokes, stories, facts, etc.

}
