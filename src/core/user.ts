export class User {

    readonly name: string;
    races: number

    constructor(name: string) {
        this.name = name;
        this.races = 0;
    }

}
