import {Room, RoomState} from "./room";
import {User} from "./user";
import {Observable} from "./observable";

export enum RaceEvent {
    Typing = "TYPING",
    Close = "CLOSE",
    Finish = "FINISH"
}

export class Race extends Observable {

    readonly room: Room;
    readonly text: string;

    readonly typing: Map<User, number>;
    private readonly _places: User[];

    starts: Date | null;
    ends: Date | null;
    readonly finished: Map<User, Date>;

    constructor(room: Room, text: string) {

        super(...Object.values(RaceEvent));

        this.room = room;
        this.text = text;
        this.typing = new Map<User, number>(room.users.map(user => [user, 0]));
        this._places = [];

        this.starts = null;
        this.ends = null;
        this.finished = new Map<User, Date>();

    }

    leave(user: User) {
        this.typing.delete(user);
        const index = this._places.indexOf(user);
        if(index !== -1) this._places.splice(index, 1);
    }

    type(user: User, char: string): number | undefined {
        if(this.typing.get(user) === undefined) return undefined;
        let typing = this.typing.get(user) as number;
        const current = this.text[typing];
        if(current === char) {
            this.typing.set(user, ++typing);
            this.emit(RaceEvent.Typing, user, typing);
            if (typing === this.text.length) {
                this.typing.delete(user);
                this._places.push(user);
                this.finished.set(user, new Date());
                this.emit(RaceEvent.Finish, user, this._places.length + 1);
                if(this.allComplete && this.room.state === RoomState.Race) this.room.endRace();
            }
            return typing;
        }
        else return undefined;
    }

    get allComplete(): boolean { return this.typing.size === 0; }

    get places(): User[] {
        const remaining = Array.from(this.typing)
            .sort(([,a], [,b]) => b - a)
            .map(pair => pair[0]);
        return [...this._places, ...remaining];
    }

}
