import fs from "fs";

export interface Repository<I> {
    getCategory(category: string): I[] | undefined
    get(category: string, index: number): I | undefined;
}

//Real Repository
export class JsonRepository<I> implements Repository<I> {

    readonly path: string;

    constructor(path: string) {
        this.path = path;
    }

    getCategory(category: string): I[] | undefined {
        const dataPath = fs.readFileSync(this.path, "utf-8");
        return JSON.parse(dataPath)[category];
    }

    get(category: string, index: number): I | undefined {
        const categoryContent = this.getCategory(category);
        return categoryContent && categoryContent[index];
    }

}

//Proxy Repository
export class CachedRepository<I> implements Repository<I> {

    private readonly repository: Repository<I>;
    private readonly cache: Map<string, I[] | undefined>;

    constructor(repository: Repository<I>) {
        this.repository = repository;
        this.cache = new Map<string, I[] | undefined>();
    }

    getCategory(category: string): I[] | undefined {
        if(!this.cache.has(category)) {
            this.cache.set(category, this.repository.getCategory(category));
        }
        return this.cache.get(category);
    }

    get(category: string, index: number): I | undefined {
        const categoryContent = this.getCategory(category);
        return categoryContent && categoryContent[index];
    }

}
