export type Event = string;

export class Observable {

    private readonly events: Map<Event, Set<Function>>
        = new Map<Event, Set<Function>>();

    constructor(...events: Event[]) {
        for(const event of events) this.events.set(event, new Set<Function>());
    }

    on(event: Event, handler: Function): void {
        if(!this.events.has(event)) throw new Error(`"${event.toString()}" doesn't exist`);
        (this.events.get(event) as Set<Function>).add(handler);
    }
    off(event: Event, handler: Function): void {
        if(!this.events.has(event)) throw new Error(`"${event.toString()}" doesn't exist`);
        (this.events.get(event) as Set<Function>).delete(handler);
    }
    emit(event: Event, ...eventArgs: any[]): void {
        if(!this.events.has(event)) throw new Error(`"${event.toString()}" doesn't exist`);
        for(const handler of this.events.get(event) as Set<Function>) handler(...eventArgs);
    }

}

export class Observer {

    private readonly subscriptions: [Observable, Event, Function][] = [];

    subscribe(observable: Observable, event: Event, handler: Function) {
        observable.on(event, handler);
        this.subscriptions.push([observable, event, handler]);
    }

    unsubscribe() {
        for(const [observable, event, handler] of this.subscriptions) observable.off(event, handler);
        this.subscriptions.splice(0, this.subscriptions.length);
    }

}
