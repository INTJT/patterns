import {User} from "./user";
import {racers, randomFromRepository} from "../data";

const randomRacer = randomFromRepository(racers);

export interface CommentatorInfo {
    readonly name: string;
    readonly profile: string;
}

export interface RacerInfo {
    readonly name: string,
    readonly races: number,
    readonly brand: string;
    readonly color: string;
    readonly description: string;
    readonly history: string;
}

export function improviseRacerInfo(user: User): RacerInfo {
    return {
        name: user.name,
        races: user.races,
        brand: randomRacer("brand"),
        color: randomRacer("color"),
        description: randomRacer("description"),
        history: randomRacer("history")
    };
}

type CarShowOptions = {
    show: boolean,
    showColor: boolean,
    showDescription: boolean
}

function formatRacerInfo(racerInfo: RacerInfo, carOptions: CarShowOptions, showHistory: boolean): string {

    const template = [racerInfo.name];

    if (carOptions.show) {
        const car = [racerInfo.brand];
        if (carOptions.showColor) car.unshift(racerInfo.color);
        if (carOptions.showDescription) car.unshift(racerInfo.description);
        template[0] += ` on ${car.join(" ")}`;
    }

    if (showHistory) template.push(racerInfo.history);

    return template.join(", ");

}

export function fullRacerInfo(racerInfo: RacerInfo): string {
    return formatRacerInfo(racerInfo, {
        show: true,
        showColor: true,
        showDescription: true
    }, true);
}

export function randomRacerInfo(racerInfo: RacerInfo): string {
    return formatRacerInfo(racerInfo, {
        show: Math.random() > 0.5,
        showColor: Math.random() > 0.5,
        showDescription: Math.random() > 0.5,
    }, false);
}
