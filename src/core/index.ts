import {User} from "./user";
import {Room, RoomEvent, RoomState} from "./room";
import {Race, RaceEvent} from "./race";
import {Commentator, CommentatorEvent} from "./commentator";
import {CommentatorFactory} from "./factory";
import {Observer} from "./observable";
import {HtmlCommentatorFactory} from "./htmlCommentator";
import {texts} from "../data";
import {SECONDS_TIMER_FOR_NOW_COMMENTS, SECONDS_TIMER_BEFORE_START_GAME, CLOSE_TYPING} from "../constants";

const htmlFactory: CommentatorFactory = new HtmlCommentatorFactory();

// Facade
export class KeyboardRaces {

    static createUser(name: string): User {
        return new User(name);
    }

    static createRoom(name: string): Room {
        return new Room(name);
    }

    static createRace(room: Room): [Race, number] {
        const category = texts.getCategory("texts") as string[];
        const index = Math.floor(Math.random() * category.length);
        return [new Race(room, category[index]), index];
    }

    static createCommentator(race: Race, callback: Function): Commentator {

        const room = race.room;

        const commentator = htmlFactory.createCommentator(race);

        const observer = new Observer();

        observer.subscribe(commentator, CommentatorEvent.Comment, callback);

        //greetings & info
        observer.subscribe(room, RoomEvent.StartTimer, () => {
            commentator.comment(commentator.greetings());
            setTimeout(() => {
                commentator.comment(commentator.info());
            }, SECONDS_TIMER_BEFORE_START_GAME * 1000 / 2);
        });

        //start
        observer.subscribe(room, RoomEvent.StartRace, () => {
           commentator.comment(commentator.start());
           periodicallyCommentNow();
           arbitrarilyCommentJoke();
        });

        //now
        function periodicallyCommentNow() {
            setTimeout(() => {
               if(room.state === RoomState.Race) {
                   commentator.comment(commentator.now());
                   periodicallyCommentNow();
               }
            }, SECONDS_TIMER_FOR_NOW_COMMENTS * 1000);
        }

        //joke
        function arbitrarilyCommentJoke() {
            setTimeout(() => {
                if(room.state === RoomState.Race) {
                    commentator.comment(commentator.joke());
                    arbitrarilyCommentJoke();
                }
            }, (10 + 7 * Math.random()) * 1000);
        }

        //close

        function checkClose(user: User, typing: number) {
            if(race.text.length - typing < CLOSE_TYPING) {
                commentator.comment(commentator.close());
                race.off(RaceEvent.Typing, checkClose);
            }
        }

        observer.subscribe(race, RaceEvent.Typing, checkClose);

        //finish
        observer.subscribe(race, RaceEvent.Finish, (user: User, place: number) => {
            commentator.comment(commentator.finish(user, place));
        });

        //leave
        observer.subscribe(room, RoomEvent.Leave, (user: User) => {
            if(room.state === RoomState.Race) commentator.comment(commentator.leave(user));
        });

        //results
        observer.subscribe(room, RoomEvent.EndRace, () => {
            commentator.comment(commentator.results());
            observer.unsubscribe();
        });

        return commentator;

    }

}

export { User, Room, Race, Commentator, CommentatorFactory };
export { RoomEvent, RaceEvent, CommentatorEvent };
export { RoomState };
export { Observer };
