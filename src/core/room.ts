import {User} from "./user";
import {Race} from "./race";
import {Observable} from "./observable";
import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME} from "../constants";

export enum RoomState {
    Waiting,
    Timer,
    Race,
    Empty
}

export enum RoomEvent {
    ChangeVisible = "CHANGE_VISIBLE",
    Join = "JOIN",
    Leave = "LEAVE",
    ChangeReady = "CHANGE_READY",
    StartTimer = "START_TIMER",
    StartRace = "START_RACE",
    EndRace = "END_RACE",
    Empty = "EMPTY"
}

export class Room extends Observable {

    private readonly _users: User[];
    private _race: Race | null;
    private _state: RoomState;
    private _visible: boolean;

    readonly name: string;
    private readonly ready: Map<User, boolean>;

    get users(): User[] { return this._users; }
    get race(): Race | null { return this._race; }
    get state(): RoomState { return this._state; }
    get visible(): boolean { return this._visible; }

    private set visible(value: boolean) {
        const previous = this._visible;
        this._visible = value;
        if(previous !== value) this.emit(RoomEvent.ChangeVisible, value);
    }

    get isEmpty() { return this._users.length === 0; }
    get isFull() { return this._users.length === MAXIMUM_USERS_FOR_ONE_ROOM; }
    get allReady() { return Array.from(this.ready.values()).every(ready => ready); }

    isReady(user: User): boolean { return this.ready.get(user) || false; }

    public constructor(name: string) {
        super(...Object.values(RoomEvent));
        this.name = name;
        this._users = [];
        this.ready = new Map<User, boolean>();
        this._state = RoomState.Waiting;
        this._race = null;
        this._visible = false;
    }

    join(user: User): void {

        if(this.state !== RoomState.Waiting) throw new Error("User can join only if room status is \"Waiting\"");
        if(this.isFull) throw new Error("Room is full");

        const isEmpty = this.isEmpty;
        this._users.push(user);
        this.ready.set(user, false);

        this.emit(RoomEvent.Join, user);
        if(isEmpty) this.visible = true;
        else if(this.isFull) this.visible = false;

    }

    leave(user: User): void {

        const index = this._users.indexOf(user);
        if(index === -1) throw new Error("User not in this room");

        this._users.splice(index, 1);
        this.ready.delete(user);
        this.emit(RoomEvent.Leave, user);

        if(this._race !== null) {
            this._race.leave(user);
            if(this._state === RoomState.Race && this._race.allComplete) this.endRace();
        }

        if(this.isEmpty) this.kill();
        else if(this.state === RoomState.Waiting) this.visible = !this.isFull;

    }

    setReady(user: User, value: boolean): void {
        if(!this._users.includes(user)) throw new Error("User isn't in the room");
        if(this._state !== RoomState.Waiting) throw new Error("Race already started");
        const previous = this.ready.get(user);
        this.ready.set(user, value);
        if(previous !== value) this.emit(RoomEvent.ChangeReady, value);
    }

    startRace(race: Race): void {
        if(!this.allReady) throw new Error("Not all users are ready");
        if(this.state !== RoomState.Waiting) throw new Error("Race already starts");
        this._race = race;
        this._state = RoomState.Timer;
        this.visible = false;
        setTimeout(() => {
            if(this.state !== RoomState.Empty) {
                this._state = RoomState.Race;
                (this._race as Race).starts = new Date();
                setTimeout(() => {
                    if(this._state !== RoomState.Waiting && this._race === race) this.endRace();
                }, SECONDS_FOR_GAME * 1000);
                this.emit(RoomEvent.StartRace, race, new Date(+new Date() + SECONDS_FOR_GAME * 1000));
            }
        }, SECONDS_TIMER_BEFORE_START_GAME * 1000);
        this.emit(RoomEvent.StartTimer, race, new Date(+new Date() + SECONDS_TIMER_BEFORE_START_GAME * 1000));
    }

    endRace(): void {
        if(this._state === RoomState.Waiting) throw new Error("There is no race");
        const places = this._race?.places;
        this._state = RoomState.Waiting;
        (this._race as Race).ends = new Date();
        this._race = null;
        for(const user of this.users) {
            user.races++;
            this.ready.set(user, false);
        }
        this.visible = !this.isEmpty && !this.isFull;
        this.emit(RoomEvent.EndRace, places);
    }

    kill(): void {
        if(this.state !== RoomState.Waiting) this.endRace();
        this._state = RoomState.Empty;
        this.visible = false;
        this.emit(RoomEvent.Empty, this);
    }

}
